import web3 from './web3';
const address = '0xf52A530Cd60c18fb5c0412489B768B47455c8652';
const abi = [
    {
      "constant": true,
      "inputs": [],
      "name": "seller",
      "outputs": [
        {
          "name": "",
          "type": "address"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function",
      "signature": "0x08551a53"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "manager",
      "outputs": [
        {
          "name": "",
          "type": "address"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function",
      "signature": "0x481c6a75"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "latestBid",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function",
      "signature": "0x5dd672ec"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "latestBidder",
      "outputs": [
        {
          "name": "",
          "type": "address"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function",
      "signature": "0x67a884e5"
    },
    {
      "inputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "constructor",
      "signature": "constructor"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "bid",
          "type": "uint256"
        }
      ],
      "name": "auction",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function",
      "signature": "0x1200617f"
    },
    {
      "constant": false,
      "inputs": [],
      "name": "bid",
      "outputs": [],
      "payable": true,
      "stateMutability": "payable",
      "type": "function",
      "signature": "0x1998aeef"
    },
    {
      "constant": false,
      "inputs": [],
      "name": "finishAuction",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function",
      "signature": "0x430ca46f"
    }
  ];
export default new web3.eth.Contract(abi, address);